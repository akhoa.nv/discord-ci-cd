class project():
	id = 0
	channelid = ''
	name = ''

	def __init__(self, id, channelid, name):
		self.id = id
		self.channelid = channelid
		self.name = name

	def get_id(self):
		return self.id

	def get_channel_id(self):
		return self.channelid

	def get_name(self):
		return self.name