import os
import configparser

class config():
	ROOT_DIR = os.path.abspath(os.curdir)
	TOKEN = None
	GITLAB_TOKEN = None
	GIT_BASE_URL = None
	ID = None

	def __init__(self):
		Config = configparser.RawConfigParser()
		Config.read(self.ROOT_DIR + '\src\config.ini')
		self.TOKEN = Config.get('Authentication', 'token')
		self.GITLAB_TOKEN = Config.get('Authentication', 'gitlab_token')
		self.GIT_BASE_URL = Config.get('Services', 'git_base_url')
		self.ID = Config.get('Bot Profile', 'id')

	def get_gitlab_token(self):
		return self.GITLAB_TOKEN

	def get_discord_token(self):
		return self.TOKEN

	def get_git_base_url(self):
		return self.GIT_BASE_URL

	def get_bot_id(self):
		return self.ID