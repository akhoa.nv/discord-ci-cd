import json
import requests
import time
import config

class core():
	repo_db = []
	repo_log = []
	web_url = []

	def __init__(self):
		pass

	def get_proj_prop(self, token, projectid, base_url):
		projectRequest = requests.get(base_url + "/api/v4/projects/" + str(projectid), headers={"PRIVATE-TOKEN": token})
		objs = json.loads(projectRequest.content.decode("utf-8"))

		return objs

	def parseRepo(self, token, projectid, base_url):
		# Emptying out db from last update
		self.repo_db = []
		self.repo_log = []
		self.web_url = []

		self.check_for_update(token, projectid, base_url)

		if (len(self.repo_db) != 0):
			self.fetch_commit_details(token, projectid, base_url)
			return self.repo_log

	def check_for_update(self, token, projectid, base_url):
		projectRequest = requests.get(base_url + "/api/v4/projects/" + str(projectid) + "/jobs", headers={"PRIVATE-TOKEN": token})
		objs = json.loads(projectRequest.content.decode("utf-8"))

		for i in range (0, len(objs) - 1):
			if (objs[i]['status'] == "created" or objs[i]['status'] == "pending" or objs[i]['status'] == "running"):
				self.repo_db.append(objs[i]['id']-1)
				self.web_url.append(objs[i-1]['web_url'])
			elif (objs[i]['status'] == "success" or objs[i]['status'] == "failed" or objs[i]['status'] == "skipped"):
				pass

	def fetch_commit_details(self, token, projectid, base_url):
		for a in range (0, len(self.repo_db) - 1):
			temp = []

			jobRequest = requests.get(base_url + "/api/v4/projects/" + str(projectid) + "/jobs/" + str(self.repo_db[a]), headers={"PRIVATE-TOKEN": token})
			jobObjs = json.loads(jobRequest.content.decode("utf-8"))

			if (jobObjs['status'] == "created" or jobObjs['status'] == "pending" or jobObjs['status'] == "running"):
				while (jobObjs['finished_at'] == None):
					jobRequest = requests.get(base_url + "/api/v4/projects/" + str(projectid) + "/jobs/" + str(self.repo_db[a]), headers={"PRIVATE-TOKEN": token})
					jobObjs = json.loads(jobRequest.content.decode("utf-8"))
					time.sleep(1)

				logRequest = requests.get(self.web_url[a] + "/raw", headers={"PRIVATE-TOKEN": token})

				replaced = logRequest.text.replace("[0K", "").replace("[0;m", "").replace("[32;1m", "").replace("[0;33m", "").replace("", "").replace("", "")
				result = self.string_process(replaced)

				
				if (len(temp) <= 0):
					temp.append(result)
				else:
					temp[0] = result
			elif (jobObjs['status'] == "success" or jobObjs['status'] == "failed"):
				logRequest = requests.get(self.web_url[a] +  "/raw", headers={"PRIVATE-TOKEN": token})

				replaced = logRequest.text.replace("[0K", "").replace("[0;m", "").replace("[32;1m", "").replace("[0;33m", "").replace("", "").replace("", "")
				result = self.string_process(replaced)

				if (len(temp) <= 0):
					temp.append(result)
				else:
					temp[0] = result

			temp.append(jobObjs['commit']['author_name'] + ' <' + jobObjs['commit']['author_email'] + '>')
			temp.append(jobObjs['commit']['id'])
			temp.append(jobObjs['commit']['message'])
			temp.append(jobObjs['commit']['created_at'])

			if (len(self.repo_log) <= a):
				self.repo_log.append(temp)
			else:
				self.repo_log[a] = temp

	def string_process(self, s):
		temp = s.split('\n')
		result = ''
		for i in range (len(temp) - 1):
			if (temp[i].startswith('section_start') or temp[i].startswith('section_end')):
				pass
			else:
				result = result + temp[i] + "\n"

		return result