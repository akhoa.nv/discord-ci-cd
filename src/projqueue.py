class projqueue():
	q = []

	def __init__(self):
		pass

	def get_obj_at(self, i):
		return self.q[i]

	def add_obj(self, obj):
		self.q.append(obj)

	def is_index_existed(self, i):
		if (i > len(self.q) or self.q[i] == None):
			return False
		else:
			return True

	def remove_obj_at_index(self, i):
		del self.q[i]

	def length(self):
		return len(self.q)