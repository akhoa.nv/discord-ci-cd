import discord
import core
import time
import asyncio
import config
import project
import projqueue
import dateutil.parser

class MyClient(discord.Client):
	conf = None
	coreFunc = core.core()
	projQ = projqueue.projqueue()

	async def on_ready(self):
		print('Logged on as {0}!'.format(self.user))
		await self.update_task()
		
	async def on_message(self, message):
		if message.content.startswith('<@' + self.conf.get_bot_id() +'> add'):
			channel = message.channel
			temp = message.content.split(' ')
			if (len(temp) <= 2 or temp[2].isdigit() == False):
				msg = ('{0.author.mention} Invalid systax. Ask me `help` for more detail').format(message)
				await channel.send(msg)
			else:
				projProp = self.coreFunc.get_proj_prop(self.conf.get_gitlab_token(), int(temp[2]), self.conf.get_git_base_url())
				tempproj = project.project(int(temp[2]), channel.id, projProp['name'])
				self.projQ.add_obj(tempproj)
				msg = ('{0.author.mention} Repository `{1}` is now on the tracking list').format(message, projProp['name'])
				await channel.send(msg)
		elif message.content.startswith('<@' + self.conf.get_bot_id() +'> help'):
			channel = message.channel
			msg = ('{0.author.mention} Mention my name and use this list of available commands (ie. `@Discord CI/CD help`):\n\n' \
			'`add`  `int projectid` - Subscribe a repository to the tracking list.\n\n' \
			'`remove`  `int projectid` - Unsubscribe a repository to the tracking list').format(message)

			await channel.send(msg)
		elif message.content.startswith('<@' + self.conf.get_bot_id() +'> hi') or message.content.startswith('<@' + self.conf.get_bot_id() +'> hello'):
			channel = message.channel
			msg = ('{0.author.mention} Howdy fellur').format(message)
			await channel.send(msg)
		elif message.content.startswith('<@' + self.conf.get_bot_id() +'>'):
			channel = message.channel
			msg = ('{0.author.mention} I don\'t know what you talkin\' \'bout dawg. I\'m hella dumb so please stick to the `help` list.').format(message)
			await channel.send(msg)
		elif ('<@' + self.conf.get_bot_id() +'>') in message.content:
			channel = message.channel
			msg = ('Look at that, I finally got mentioned for something').format()
			await channel.send(msg)
			
	async def update_task(self):
		while True:
			if (self.projQ.length() == 0):
				pass
			elif (self.projQ.length() == 1):
				channel = discord.Client.get_channel(self, self.projQ.get_obj_at(0).get_channel_id())
				repo_post = await self.obtain_obj(self.projQ.get_obj_at(0).get_id())
				if (repo_post != None and len(repo_post) != 0):
					if (len(repo_post) == 1):
						msg = "commit {}\nAuthor: {}\nDate: {}\n\nMessage:\n```\n{}\n```\nBuild log:\n```\n{}\n```".format(repo_post[0][2], repo_post[0][1], dateutil.parser.parse(repo_post[0][4]), repo_post[0][3], repo_post[0][0])
						await channel.send(msg)
					else:
						for a in range (0, len(repo_post) - 1):
							msg = "commit {}\nAuthor: {}\nDate: {}\n\nMessage:\n```\n{}\n```\nBuild log:\n```\n{}\n```".format(repo_post[a][2], repo_post[a][1], dateutil.parser.parse(repo_post[0][4]), repo_post[a][3], repo_post[a][0])
							await channel.send(msg)
				else:
					print("No new commit")
			else:
				for i in range (self.projQ.length() - 1):
					channel = discord.Client.get_channel(self, self.projQ.get_obj_at(i).get_channel_id())
					repo_post = await self.obtain_obj(self.projQ.get_obj_at(i).get_id())
					if (repo_post != None and len(repo_post) != 0):
						if (len(repo_post) == 1):
							msg = "commit {}\nAuthor: {}\nDate: {}\n\nMessage:\n```\n{}\n```\nBuild log:\n```\n{}\n```".format(repo_post[0][2], repo_post[0][1], dateutil.parser.parse(repo_post[0][4]), repo_post[0][3], repo_post[0][0])
							await channel.send(msg)
						else:
							for a in range (0, len(repo_post) - 1):
								msg = "commit {}\nAuthor: {}\nDate: {}\n\nMessage:\n```\n{}\n```\nBuild log:\n```\n{}\n```".format(repo_post[a][2], repo_post[a][1], dateutil.parser.parse(repo_post[0][4]), repo_post[a][3], repo_post[a][0])
								await channel.send(msg)
					else:
						print("No new commit")
			await asyncio.sleep(0.1*60)

	async def obtain_obj(self, i):
		repo_post = self.coreFunc.parseRepo(self.conf.get_gitlab_token(), i, self.conf.get_git_base_url())
		return repo_post

	def set_conf_obj(self, conf):
		self.conf = conf

def init():
	conf = config.config()

	client = MyClient()
	client.set_conf_obj(conf)
	client.run(conf.get_discord_token())

init()