# Discord CI/CD

### About
Author: Khoa Nguyen

Please refer to the official repo if there're any issues: https://git.nguyenrepo.space/akhoa.nv/discordcicd


### Concept
- Automatically fetch any new commits from added git repositories on Discord channel
- Shows commits' detail
- Shows automatic build system's log

### Dependencies
- discord.py (`pip install discord.py`)
- configparser (`pip install configparser`)

### Deploy
- run `python src/server.py` in project root